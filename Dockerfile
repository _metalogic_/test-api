FROM golang:1.19 as builder

COPY ./ /build

WORKDIR /build/cmd/server

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o test-api .

WORKDIR /build/cmd/tester

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o tester .

FROM metalogic/alpine:3.15

RUN adduser -u 25000 -g 'Metalogic Application Runner' -D runner

WORKDIR /home/runner

COPY --from=builder /build/cmd/server/test-api .

COPY --from=builder /build/cmd/tester/tester /usr/local/bin

USER runner

CMD ["./test-api"]

