package main

import (
	"flag"
	"os"

	"bitbucket.org/_metalogic_/config"
	"bitbucket.org/_metalogic_/log"
	"bitbucket.org/_metalogic_/test-api/http"
	"bitbucket.org/_metalogic_/test-api/mssql"
)

var (
	env      string
	levelFlg log.Level
	portFlg  string

	dbname     string
	dbhost     string
	dbport     string
	dbuser     string
	dbpassword string
)

func init() {
	flag.Var(&levelFlg, "level", "set level of logging by the API")
	flag.StringVar(&portFlg, "port", ":8080", "listen port")
}

func main() {
	flag.Parse()

	var err error
	// get config from Docker secrets or environment
	dbuser = config.MustGetConfig("API_DB_USER")
	dbpassword = config.MustGetConfig("API_DB_PASSWORD")
	dbhost = config.MustGetenv("DB_HOST")
	dbport = config.IfGetenv("DB_PORT", "1433")
	dbname = config.MustGetenv("DB_NAME")

	if levelFlg != log.None {
		log.SetLevel(levelFlg)
	} else {
		loglevel := os.Getenv("LOG_LEVEL")
		if loglevel == "DEBUG" {
			log.SetLevel(log.DebugLevel)
		}
	}

	// set encryption key (needed if some or all secret files are encrypted)
	config.DecryptionKey = config.MustGetConfig("GLOBAL_ENCRYPTION_KEY")

	testService, err := mssql.New(dbhost, dbport, dbname, dbuser, dbpassword)
	if err != nil {
		log.Fatalf("failed to create test-api Service: %s\n", err)
	}
	defer testService.Close()

	handler := http.NewHandler(testService)

	log.Warning("started test-api")

	log.Fatal(handler.ServeHTTP(portFlg))
}
