package main

import (
	"flag"
	"fmt"

	"bitbucket.org/_metalogic_/config"
)

func main() {

	flag.Parse()

	name := flag.Arg(0)

	val := config.MustGetConfig(name)

	fmt.Printf("MustGetConfig: %s = %s\n", name, val)
}
