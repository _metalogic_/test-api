-- +goose NO TRANSACTION
-- +goose Up
CREATE TABLE [test].[POST]
(
    id int NOT NULL,
    title text,
    body text,
    PRIMARY KEY(id)
);

-- +goose Down
DROP TABLE [test].[POST];
