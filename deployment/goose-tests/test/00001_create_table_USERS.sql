-- +goose Up
CREATE TABLE [test].[USERS]
(
    id int NOT NULL PRIMARY KEY,
    username varchar(200),
    name nvarchar(max),
    surname nvarchar(max)
);

INSERT INTO [test].[USERS]
VALUES
    (0, 'root', '', ''),
    (1, 'vojtechvitek', 'Vojtech', 'Vitek');

-- +goose Down
DROP TABLE test.users;
