-- +goose Up
-- +goose StatementBegin
UPDATE [test].[USERS] SET username='admin' WHERE username='root';
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
UPDATE [test].[USERS] SET username='root' WHERE username='admin';
-- +goose StatementEnd
