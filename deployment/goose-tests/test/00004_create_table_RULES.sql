-- +goose Up

-- +goose StatementBegin

CREATE TABLE [test].[RULES]
(
    ID INT NOT NULL IDENTITY,
    PreviousID INT,
    InstitutionID INT NOT NULL,
    GUID VARCHAR(100) DEFAULT newid() NOT NULL,
    Description NVARCHAR(256) NOT NULL,
    Created DATETIME CONSTRAINT DF_test_RULES_Created DEFAULT getdate() NOT NULL,
    CreateUser VARCHAR(50) CONSTRAINT DF_test_RULES_CreateUser DEFAULT 'ROOT' NOT NULL,
    Updated DATETIME CONSTRAINT DF_test_RULES_Updated DEFAULT getdate() NOT NULL,
    UpdateUser VARCHAR(50) CONSTRAINT DF_test_RULES_UpdateUser DEFAULT 'ROOT' NOT NULL,
    Expr VARCHAR(8000) CONSTRAINT DF_test_RULES_Expr DEFAULT 'false' NOT NULL,
    Name VARCHAR(50) NOT NULL,
    Status VARCHAR(10) CONSTRAINT DF_test_RULES_Status DEFAULT 'DRAFT' NOT NULL,
    ArchiveDate DATETIME,
    AST VARCHAR(MAX) CONSTRAINT DF_test_RULES_AST DEFAULT '{ "type": "LITERAL", "value": false}' NOT NULL,
    EvalType VARCHAR(8) CONSTRAINT DF_test_RULES_EvalType DEFAULT 'EXPR' NOT NULL,
    PRIMARY KEY (ID),
    CONSTRAINT FK_test_RULES_PreviousID FOREIGN KEY (PreviousID) REFERENCES [test].[RULES] ("ID"),
    CONSTRAINT FK_test_RULES_InstitutionID FOREIGN KEY (InstitutionID) REFERENCES [inst].[INSTITUTIONS] ("ID"),
    CONSTRAINT UQ_test_RULES_GUID UNIQUE (GUID),
    CONSTRAINT UQ_test_RULES_Name_Status_ArchiveDate UNIQUE (Name, InstitutionID, Status, ArchiveDate),
    CONSTRAINT CHK_test_RULES_Status CHECK ([Status]='DRAFT' OR [Status]='FINAL' OR [Status]='ARCHIVED')
);

-- +goose StatementEnd

-- +goose Down

DROP TABLE [test].[RULES];