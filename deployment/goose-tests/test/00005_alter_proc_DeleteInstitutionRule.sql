-- +goose Up

-- +goose StatementBegin

CREATE OR ALTER PROCEDURE [test].[DeleteInstitutionRule]
    @SessionGUID VARCHAR(50),
    @epbcID VARCHAR(150),
    @RuleGUID VARCHAR(50)
WITH
    EXEC AS CALLER
AS
BEGIN
    DECLARE @BaseCode INT = 50000;
    DECLARE @ReturnCode INT;
    DECLARE @Message VARCHAR(200);

    BEGIN TRY
    DECLARE @InstitutionID INT;
    SELECT @InstitutionID = ID
    FROM [inst].[INSTITUTIONS]
    WHERE EPBCID = @EPBCID;

    IF @InstitutionID IS NULL
    BEGIN
        SET @ReturnCode = @BaseCode + 404;
        SET @Message = 'institution not found with EPBCID = ' + @EPBCID;
        THROW @ReturnCode, @Message, 1;
    END;

    DECLARE @RuleID INT;
    SELECT @RuleID = ID
    FROM [test].[RULES]
    WHERE InstitutionID = @InstitutionID AND GUID = @RuleGUID;


    IF @RuleID IS NULL
    BEGIN
        SET @ReturnCode = @BaseCode + 404;
        SET @Message = 'rule not found for institution ' + @EPBCID + ' with GUID ' + @RuleGUID;
        THROW @ReturnCode, @Message, 1;
    END;

    -- The logic implemented by the backend is as follows:
    --
    -- if the RULE is in DRAFT state execute the following in a single transaction:
    --    update the RuleID of each referencing (DRAFT) child object to NULL
    --    delete the RULE identified by ruleGUID
    -- if the RULE is in FINAL state execute the following in a single transaction:
    --    set the status of each child object to ARCHIVED
    --    set the status of the RULE to archived
    --    QUESTION - did we decide that we should create copies of each child object affected
    --
    -- The QUESTION above reflects my thinking that the real importance of archiving is in FEES and QUESTIONS not so much RULES.
    -- There is no harm in setting a FEE or QUESTION to ARCHIVED while leaving the referenced RULE in state FINAL.
    -- It seems to me the usual reason to set a RULE to ARCHIVED status is to prevent it from being used in new FEES or QUESTIONS.
    -- In this case, the RULE is probably not being used by current FINAL QUESTIONS or FEES. In other words, setting the RULE 
    -- to status ARCHIVED is most common when deleting the RULE.
    --
    -- I think it might be reasonable (at least for V1) to disallow setting a RULE to ARCHIVED unless all of its child objects are already ARCHIVED. 

    BEGIN TRANSACTION
    SAVE TRANSACTION DeleteRule;

    DECLARE @CURR_STATUS VARCHAR(10);
    SELECT @CURR_STATUS = Status
    FROM [test].[RULES]
    WHERE ID=@RuleID;

    IF @CURR_STATUS='ARCHIVED'
    BEGIN
        SET @ReturnCode = @BaseCode + 400;
        SET @Message = 'cannot delete ARCHIVED rule for institution ' + @EPBCID + ' with rule guid ' + @RuleGUID;
        THROW @ReturnCode, @Message, 1;
    END;

    -- if the RULE is in FINAL state execute the following in a single transaction:
    --    set the status of each child object to ARCHIVED
    --    set the status of the RULE to archived
    IF @CURR_STATUS='FINAL'
    BEGIN
        -- changing a FINAL status RULE to ARCHIVED forces all child objects to also be set to ARCHIVED
        -- an ARCHIVED status RULE may only be referenced by child objects also in ARCHIVED state
        -- for V1 disallow setting a RULE to ARCHIVED unless all of its child objects are already ARCHIVED
        IF ( (SELECT ID
            FROM fees.[FEE_TYPES]
            WHERE RuleID=@RuleID AND Status!='ARCHIVED'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT QuestionID
            FROM bbq.[QUESTIONS]
            WHERE RuleID=@RuleID AND Status!='ARCHIVED'
            ORDER BY QuestionID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[ADMISSION_CATEGORIES]
            WHERE RuleID=@RuleID AND Status!='ARCHIVED'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[CAMPUSES]
            WHERE RuleID=@RuleID AND Status!='ARCHIVED'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[FACULTIES]
            WHERE RuleID=@RuleID AND Status!='ARCHIVED'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[PROGRAM_OFFERINGS]
            WHERE RuleID=@RuleID AND Status!='ARCHIVED'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[PROGRAMS]
            WHERE RuleID=@RuleID AND Status!='ARCHIVED'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[TERMS]
            WHERE RuleID=@RuleID AND Status!='ARCHIVED'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            )
        BEGIN
            SET @ReturnCode = @BaseCode + 400;
            SET @Message = 'cannot ARCHIVE rule until all database objects using it are also archived for institution ' + @EPBCID + ' with rule guid ' + @RuleGUID;
            THROW @ReturnCode, @Message, 1;
        END;

        UPDATE [test].[RULES]
        SET
            [Status]='ARCHIVED',
            [UpdateUser] = @SessionGUID,
            [ArchiveDate] = getdate()
        WHERE ID=@RuleID;
    END;

    -- if the RULE is in DRAFT state execute the following in a single transaction:
    --    update the RuleID of each referencing (DRAFT) child object to NULL
    --    delete the RULE identified by ruleGUID
    IF @CURR_STATUS='DRAFT'
    BEGIN
        -- a DRAFT rule may only be referenced by child objects that are also in DRAFT status
        IF ( (SELECT ID
            FROM fees.[FEE_TYPES]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT QuestionID
            FROM bbq.[QUESTIONS]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY QuestionID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[ADMISSION_CATEGORIES]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[CAMPUSES]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[FACULTIES]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[PROGRAM_OFFERINGS]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[PROGRAMS]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[TERMS]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            )
        BEGIN
            SET @ReturnCode = @BaseCode + 500;
            SET @Message = 'the DRAFT rule for institution ' + @EPBCID + ' with rule guid ' + @RuleGUID + ' is referenced by database objects that are not in DRAFT which is forbidden';
            THROW @ReturnCode, @Message, 1;
        END;

        UPDATE fees.[FEE_TYPES]
        SET
            [RuleID]=NULL,
            [LastUpdateUser]=@SessionGUID,
            [LastUpdated]=getdate()
        WHERE RuleID=@RuleID;

        UPDATE bbq.[QUESTIONS]
        SET 
            [RuleID]=NULL,
            [UpdateUser]=@SessionGUID,
            [Updated]=getdate()
         WHERE RuleID=@RuleID;

        UPDATE progsel.[ADMISSION_CATEGORIES]
        SET
            [RuleID]=NULL,
            [UpdateUser]=@SessionGUID,
            [Updated]=getdate()
        WHERE RuleID=@RuleID;

        UPDATE progsel.[CAMPUSES]
        SET
            [RuleID]=NULL,
            [UpdateUser]=@SessionGUID,
            [Updated]=getdate()
        WHERE RuleID=@RuleID;

        UPDATE progsel.[FACULTIES]
        SET
            [RuleID]=NULL,
            [UpdateUser]=@SessionGUID,
            [Updated]=getdate()
        WHERE RuleID=@RuleID;

        UPDATE progsel.[PROGRAM_OFFERINGS]
        SET
            [RuleID]=NULL,
            [UpdateUser]=@SessionGUID,
            [Updated]=getdate()
        WHERE RuleID=@RuleID;

        UPDATE progsel.[PROGRAMS]
        SET
            [RuleID]=NULL,
            [UpdateUser]=@SessionGUID,
            [Updated]=getdate()
        WHERE RuleID=@RuleID;

        UPDATE progsel.[TERMS]
        SET
            [RuleID]=NULL,
            [UpdateUser]=@SessionGUID,
            [Updated]=getdate()
        WHERE RuleID=@RuleID;

        DELETE FROM [test].[RULES] WHERE ID = @RuleID;
    END;

    COMMIT TRANSACTION DeleteRule;

    SELECT '{"result": "Success"}';

    END TRY

    BEGIN CATCH
    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION DeleteRule;

    IF ERROR_NUMBER() > 50000
        THROW;

    DECLARE @ErrorMessage VARCHAR(400);
    SELECT @ErrorMessage = 'Delete rule transaction aborted for rule '  + @RuleGUID + ' on EPBCID ' + @EPBCID + ': '  + ERROR_MESSAGE();
    THROW 50000, @ErrorMessage, 1;
    END CATCH
END;

-- +goose StatementEnd   

-- +goose Down

DROP PROCEDURE [test].DeleteInstitutionRule;

