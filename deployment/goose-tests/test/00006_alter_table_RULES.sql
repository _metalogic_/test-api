-- +goose Up

-- AST column holds JSON Abstract Syntax Tree

-- +goose StatementBegin
IF COLUMNPROPERTY(OBJECT_ID('test.RULES'), 'AST', 'ColumnId') IS NULL
BEGIN
    ALTER TABLE [test].[RULES] ADD AST VARCHAR(MAX) NOT NULL CONSTRAINT DF_test_RULES_AST DEFAULT '{"type": "LITERAL", "value": false}';
END;

-- EvalType records whether to evaluate the rule by AST or EXPR
IF COLUMNPROPERTY(OBJECT_ID('test.RULES'), 'EvalType', 'ColumnId') IS NULL
BEGIN
    ALTER TABLE [test].[RULES] ADD EvalType VARCHAR(8) NOT NULL CONSTRAINT DF_test_RULES_EvalType DEFAULT 'EXPR';
END;
-- +goose StatementEnd   

-- +goose Down

-- +goose StatementBegin  
IF COLUMNPROPERTY(OBJECT_ID('test.RULES'), 'AST', 'ColumnId') IS NOT NULL
BEGIN
    ALTER TABLE [test].[RULES] DROP CONSTRAINT DF_test_RULES_AST;
    ALTER TABLE [test].[RULES] DROP COLUMN AST;
END;

-- EvalType records whether to evaluate the rule by AST or EXPR
IF COLUMNPROPERTY(OBJECT_ID('test.RULES'), 'EvalType', 'ColumnId') IS NOT NULL
BEGIN
    ALTER TABLE [test].[RULES] DROP CONSTRAINT DF_test_RULES_EvalType;
    ALTER TABLE [test].[RULES] DROP COLUMN EvalType;
END;
-- +goose StatementEnd   