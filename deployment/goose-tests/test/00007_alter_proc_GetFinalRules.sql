-- +goose Up

-- +goose StatementBegin
CREATE OR ALTER PROCEDURE [test].[GetFinalRules]
AS
BEGIN
  DECLARE @BaseCode INT = 50000
  DECLARE @ReturnCode INT
  DECLARE @Message VARCHAR(200)
  BEGIN TRY
  DECLARE @rules NVARCHAR(max)
  SET @rules = (
    SELECT [r].GUID AS "guid",
    [r].Name AS "name",
    [r].Description AS "description",
    [r].AST AS "ast",
    [r].Expr AS "expr",
    [r].EvalType AS "evalType",
    [r].Status AS "status", [i].[EPBCID] AS "epbcID",
    FORMAT([r].Created, N'yyyy-MM-ddTHH:mm:ssZ') AS "updateDetails.created",
    [r].CreateUser AS "updateDetails.createUser",
    FORMAT([r].Updated, N'yyyy-MM-ddTHH:mm:ssZ') AS "updateDetails.updated",
    [r].UpdateUser AS "updateDetails.updateUser"
  FROM [test].[RULES] [r]
    JOIN [inst].[INSTITUTIONS] [i] ON [r].InstitutionID = [i].ID
  WHERE [r].Status = 'FINAL'
  FOR JSON PATH, INCLUDE_NULL_VALUES)

  SELECT ISNULL(@rules, '[]')

  END TRY
  
  BEGIN CATCH
    IF ERROR_NUMBER() > 50000
      THROW;

    DECLARE @ErrorMessage VARCHAR(400)
    SELECT @ErrorMessage = 'get final rules failed: ' + ERROR_MESSAGE();
    THROW 50000, @ErrorMessage, 1; 
  END CATCH
END;
-- +goose StatementEnd

-- +goose Down

DROP PROCEDURE [test].[GetFinalRules];