
-- +goose Up

-- +goose StatementBegin
CREATE OR ALTER PROCEDURE [test].[GetInstitutionRule]
	@EPBCID VARCHAR(50),
	@GUID VARCHAR(50)
WITH
	EXEC AS CALLER
AS
BEGIN
	DECLARE @BaseCode INT = 50000
	DECLARE @ReturnCode INT
	DECLARE @Message VARCHAR(200)

	BEGIN TRY
  	DECLARE @InstitutionID INT
  	SELECT @InstitutionID = ID
	FROM [inst].[INSTITUTIONS]
	WHERE EPBCID = @EPBCID
    
  	IF @InstitutionID IS NULL
  	BEGIN
		SET @ReturnCode = @BaseCode + 404;
		SET @Message = 'institution not found with EPBCID ' + @EPBCID;
		THROW @ReturnCode, @Message, 1;
	END
    
  	DECLARE @RuleID INT
  	SELECT @RuleID = ID
	FROM [test].[RULES]
	WHERE institutionID = @InstitutionID AND GUID = @GUID
  
  	IF @RuleID IS NULL
  	BEGIN
		SET @ReturnCode = @BaseCode + 404;
		SET @Message = 'rule not found for institution ' + @EPBCID + ' with GUID ' + @GUID;
		THROW @ReturnCode, @Message, 1;
	END
    
  	SELECT [r].GUID AS "guid",
		[r].Name AS "name",
		[r].Description AS "description",
		[r].AST AS "ast",
		[r].Expr AS "expr",
		[r].EvalType AS "evalType",
		[r].Status AS "status",
		FORMAT([r].Created, N'yyyy-MM-ddTHH:mm:ssZ') AS "updateDetails.created", [r].CreateUser AS "updateDetails.createUser",
		FORMAT([r].Updated, N'yyyy-MM-ddTHH:mm:ssZ') AS "updateDetails.updated", [r].UpdateUser AS "updateDetails.updateUser"
	FROM [test].[RULES] [r]
	WHERE [r].ID = @RuleID
	FOR JSON PATH, WITHOUT_ARRAY_WRAPPER

  	END TRY
  
  	BEGIN CATCH
  	IF ERROR_NUMBER() > 50000
      THROW;
    
  	DECLARE @ErrorMessage VARCHAR(400)
  	SELECT @ErrorMessage = 'Get rule failed for institution ' + @EPBCID + ' with GUID ' + @GUID + ': ' + ERROR_MESSAGE();
  	THROW 50000, @ErrorMessage, 1;
  	END CATCH

END;

-- +goose StatementEnd

-- +goose Down

DROP PROCEDURE [test].[GetInstitutionRule];