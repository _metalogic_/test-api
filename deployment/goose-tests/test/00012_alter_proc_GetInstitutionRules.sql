-- +goose Up

-- +goose StatementBegin

CREATE OR ALTER PROCEDURE [test].[GetInstitutionRules]
  @EPBCID VARCHAR(50),
  @Name VARCHAR(50) = NULL,
  @Status VARCHAR(10) = NULL,
  @Extended BIT
AS
BEGIN
  DECLARE @BaseCode INT = 50000
  DECLARE @ReturnCode INT
  DECLARE @Message VARCHAR(200)
  BEGIN TRY
    DECLARE @InstitutionID INT
    SELECT @InstitutionID = ID
  FROM [inst].[INSTITUTIONS]
  WHERE EPBCID = @EPBCID

    IF @InstitutionID IS NULL
    BEGIN
    SET @ReturnCode = @BaseCode + 404;
    SET @Message = 'institution not found with EPBCID ' + @EPBCID;
    THROW @ReturnCode, @Message, 1;
  END
    
    DECLARE @rules NVARCHAR(max)
    IF @Extended = 0
    BEGIN
    SET @rules = (
      SELECT [r].GUID AS "guid",
      [i].epbcID AS "institution.epbcID",
      [i].shortName AS "institution.shortName",
      [r].Name AS "name",
      [r].Description AS "description",
      [r].AST AS "ast",
      [r].Expr AS "expr",
      [r].EvalType AS "evalType",
      [r].Status AS "status",
      FORMAT([r].Created, N'yyyy-MM-ddTHH:mm:ssZ') AS "updateDetails.created",
      [r].CreateUser AS "updateDetails.createUser",
      FORMAT([r].Updated, N'yyyy-MM-ddTHH:mm:ssZ') AS "updateDetails.updated",
      [r].UpdateUser AS "updateDetails.updateUser"
    FROM [test].[RULES] [r]
      INNER JOIN [inst].[INSTITUTIONS] [i] ON [i].ID = [r].InstitutionID
    WHERE [r].InstitutionID = @InstitutionID
      AND (@Name IS NULL OR [r].[Name] = @Name)
      AND (@Status IS NULL OR [r].[Status] = @Status)
    FOR JSON PATH, INCLUDE_NULL_VALUES)
  END
    ELSE
    BEGIN
    SET @rules = (
      SELECT [r].GUID AS "guid",
      [i].epbcID AS "institution.epbcID",
      [i].shortName AS "institution.shortName",
      [r].Name AS "name",
      [r].Description AS "description",
      [r].AST AS "ast",
      [r].Expr AS "expr",
      [r].EvalType AS "evalType",
      [r].Status AS "status",
      FORMAT([r].Created, N'yyyy-MM-ddTHH:mm:ssZ') AS "updateDetails.created",
      [r].CreateUser AS "updateDetails.createUser",
      FORMAT([r].Updated, N'yyyy-MM-ddTHH:mm:ssZ') AS "updateDetails.updated",
      [r].UpdateUser AS "updateDetails.updateUser"
    FROM [test].[RULES] [r]
      INNER JOIN [inst].[INSTITUTIONS] [i] ON [i].ID = [r].InstitutionID
    WHERE [r].InstitutionID IN (1, @InstitutionID)
      AND (@Name IS NULL OR [r].[Name] = @Name)
      AND (@Status IS NULL OR [r].[Status] = @Status)
    FOR JSON PATH, INCLUDE_NULL_VALUES)
  END
    
    SELECT ISNULL(@rules, '[]')
  END TRY
  
  BEGIN CATCH
    IF ERROR_NUMBER() > 50000
      THROW;
    DECLARE @ErrorMessage VARCHAR(400)
    SELECT @ErrorMessage = 'get institution rules failed for ' + @EPBCID + ': ' + ERROR_MESSAGE();
    THROW 50000, @ErrorMessage, 1; 
  END CATCH
END;

-- +goose StatementEnd

-- +goose Down

DROP PROCEDURE [test].[GetInstitutionRules];

