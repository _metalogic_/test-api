-- +goose Up

-- +goose StatementBegin

CREATE OR ALTER PROCEDURE [test].[GetRuleUsage]
    @RuleGUID VARCHAR(50),
    @UsageType VARCHAR(20) = NULL,
    @UsageStatus VARCHAR(10) = NULL
WITH
    EXEC AS CALLER
AS
BEGIN
    DECLARE @BaseCode INT = 50000
    DECLARE @ReturnCode INT
    DECLARE @Message VARCHAR(200)

    BEGIN TRY
            
    DECLARE @RuleID int

    SELECT @RuleID=ID
    FROM [test].[RULES]
    WHERE GUID = @RuleGUID
    
    IF @RuleID IS NULL
    BEGIN
        SET @ReturnCode = @BaseCode + 404;
        SET @Message = 'rule not found with guid ' + @RuleGUID;
        THROW @ReturnCode, @Message, 1;
    END

    IF @UsageType IS NOT NULL AND @UsageType NOT IN ('question', 'fee', 'admissioncategory', 'campus', 'faculty', 'programoffering', 'program', 'term')
    BEGIN
        SET @ReturnCode = @BaseCode + 404;
        Set @Message = 'query parameter Type if present must be one of question, fee, admissioncategory, campus, faculty, programoffering, program or term';
        THROW @ReturnCode, @Message, 1;
    END

    IF @UsageStatus IS NOT NULL AND @UsageStatus NOT IN ('FINAL', 'DRAFT', 'ARCHIVED')
    BEGIN
        SET @ReturnCode = @BaseCode + 404;
        Set @Message = 'query parameter Status if present must be one of DRAFT, FINAL or ARCHIVED';
        THROW @ReturnCode, @Message, 1;
    END

    DECLARE @ruleInfo NVARCHAR(max)
    SET @ruleInfo =
    (SELECT
        --'rule' AS "type",
        [i].[EPBCID] AS "epbcId",
        [r].[GUID] AS "guid",
        [r].[Name] AS "name",
        [r].[Description] AS "description",
        [r].[AST] AS "ast",
        [r].[Expr] AS "expr",
        [r].[EvalType] AS "evalType",
        [r].[Status] AS "status"
    FROM [test].[RULES] [r]
        JOIN [inst].[INSTITUTIONS] [i] on [r].[InstitutionID] = [i].[ID]
    WHERE [r].[GUID]= @RuleGUID
    FOR JSON PATH, INCLUDE_NULL_VALUES)

    SET @ruleInfo = ISNULL(@ruleInfo, '[]')

    DECLARE @questions NVARCHAR(max)    	
    SET @questions =
    (SELECT
        --'questions' AS "type",
        --NULL AS "subtype",
        [i].[EPBCID] AS "epbcId",
        [q].[GUID] AS "guid",
        [q].[QuestionKey] AS "code",
        [q].[QuestionText] AS "name",
        [q].[Status] AS "status"
    FROM [bbq].[QUESTIONS] [q]
        JOIN [inst].[INSTITUTIONS] [i] ON [q].[InstitutionEpbcId] = [i].[EPBCID]
    WHERE [q].[RuleID] = @RuleID
        AND (@UsageType IS NULL OR @UsageType='question')
        AND (@UsageStatus IS NULL OR @UsageStatus = [q].[Status])
    FOR JSON PATH, INCLUDE_NULL_VALUES)
                
    SET @questions = ISNULL(@questions, '[]')

    DECLARE @fees NVARCHAR(max)    	
    SET @fees = (
    SELECT
        --'fees' AS "type",
        --NULL AS "subtype",
        [i].[EPBCID] AS "epbcId",
        [ft].[GUID] AS "guid",
        NULL AS "code",
        [ft].[Description] AS "name",
        [ft].[Status] AS "status"
    FROM [fees].[FEE_TYPES] [ft]
        JOIN [inst].[INSTITUTIONS] [i] ON [ft].InstitutionID = [i].[ID]
    WHERE [ft].[RuleID] = @RuleID
        AND (@UsageType IS NULL OR @UsageType='fee')
        AND (@UsageStatus IS NULL OR @UsageStatus = [ft].[Status])
    FOR JSON PATH, INCLUDE_NULL_VALUES)
                 
    SET @fees = ISNULL(@fees, '[]')

    DECLARE @admissioncategories NVARCHAR(max)
    SET @admissioncategories = (
    SELECT
        --'programselection' AS "type",
        --'admissioncategory' AS "subtype",
        [i].[EPBCID] AS "epbcId",
        [ac].[GUID] AS "guid",
        [ac].[Code] AS "code",
        [ac].[Name] AS "name",
        [ac].[Status] AS "status"
    FROM [progsel].[ADMISSION_CATEGORIES] [ac]
        JOIN [inst].[INSTITUTIONS] [i] ON [ac].InstitutionID = [i].[ID]
    WHERE [ac].[RuleID] = @RuleID
        AND (@UsageType IS NULL OR @UsageType='admissioncategory')
        AND (@UsageStatus IS NULL OR @UsageStatus = [ac].[Status])
    FOR JSON PATH, INCLUDE_NULL_VALUES)
        
    SET @admissioncategories = ISNULL(@admissioncategories, '[]')
    
    DECLARE @campuses NVARCHAR(max)
    SET @campuses = (
    SELECT
        --'programselection' AS "type",
        --'campus' AS "subtype",
        [i].[EPBCID] AS "epbcId",
        [c].[GUID] AS "guid",
        [c].[Code] AS "code",
        [c].[Name] AS "name",
        [c].[Status] AS "status"
    FROM [progsel].[CAMPUSES] [c]
        JOIN [inst].[INSTITUTIONS] [i] ON [c].InstitutionID = [i].[ID]
    WHERE [c].[RuleID] = @RuleID
        AND (@UsageType IS NULL OR @UsageType='campus')
        AND (@UsageStatus IS NULL OR @UsageStatus = [c].[Status])
    FOR JSON PATH, INCLUDE_NULL_VALUES)
        
    SET @campuses = ISNULL(@campuses, '[]')
     
    DECLARE @faculties NVARCHAR(max)
    SET @faculties = (
    SELECT
        --'programselection' AS "type",
        --'faculty' AS "subtype",
        [i].[EPBCID] AS "epbcId",
        [f].[GUID] AS "guid",
        [f].[Code] AS "code",
        [f].[Name] AS "name",
        [f].[Status] AS "status"
    FROM [progsel].[FACULTIES] [f]
        JOIN [inst].[INSTITUTIONS] [i] ON [f].InstitutionID = [i].[ID]
    WHERE [f].[RuleID] = @RuleID
        AND (@UsageType IS NULL OR @UsageType='faculty')
        AND (@UsageStatus IS NULL OR @UsageStatus = [f].[Status])
    FOR JSON PATH, INCLUDE_NULL_VALUES)
        
    SET @faculties = ISNULL(@faculties, '[]')

    DECLARE @programofferings NVARCHAR(max)
    SET @programofferings = (
    SELECT
        --'programselection' AS "type",
        --'programoffering' AS "subtype",
        NULL AS "epbcId",
        [po].[GUID] AS "guid",
        [p].[Code] AS "code",
        [p].[Name] AS "name",
        [po].[Status] AS "status"
    FROM [progsel].[PROGRAM_OFFERINGS] [po]
        LEFT JOIN [progsel].[PROGRAMS] p ON po.ProgramID = p.ID
    WHERE [po].[RuleID] = @RuleID
        AND (@UsageType IS NULL OR @UsageType='programoffering')
        AND (@UsageStatus IS NULL OR @UsageStatus = [po].[Status])
    FOR JSON PATH, INCLUDE_NULL_VALUES)
    
    SET @programofferings = ISNULL(@programofferings, '[]')

        
    DECLARE @programs NVARCHAR(max)
    SET @programs = (
    SELECT
        --'programselection' AS "type",
        --'program' AS "subtype",
        [i].[EPBCID] AS "epbcId",
        [p].[GUID] AS "guid",
        [p].[Code] AS "code",
        [p].[Name] AS "name",
        [p].[Status] AS "status"
    FROM [progsel].[PROGRAMS] [p]
        JOIN [inst].[INSTITUTIONS] [i] ON [p].InstitutionID = [i].[ID]
    WHERE [p].[RuleID] = @RuleID
        AND (@UsageType IS NULL OR @UsageType='program')
        AND (@UsageStatus IS NULL OR @UsageStatus = [p].[Status])
    FOR JSON PATH, INCLUDE_NULL_VALUES)
    
    SET @programs = ISNULL(@programs, '[]')

    DECLARE @terms NVARCHAR(max)
    SET @terms = (
    SELECT
        --'programselection' AS "type",
        --'term' AS "subtype",
        [i].[EPBCID] AS "epbcId",
        [t].[GUID] AS "guid",
        [t].[Code] AS "code",
        [t].[Name] AS "name",
        [t].[Status] AS "status"
    FROM [progsel].[TERMS] [t]
        JOIN [inst].[INSTITUTIONS] [i] ON [t].InstitutionID = [i].[ID]
    WHERE [t].[RuleID] = @RuleID
        AND (@UsageType IS NULL OR @UsageType='term')
        AND (@UsageStatus IS NULL OR @UsageStatus = [t].[Status])
    FOR JSON PATH, INCLUDE_NULL_VALUES)
        
    SET @terms = ISNULL(@terms, '[]')

    DECLARE @json NVARCHAR(max)
    IF @UsageType IS NULL
    BEGIN
        SET @json = (SELECT JSON_QUERY(@ruleInfo) AS "ruleInfo",
            JSON_QUERY(@questions) AS "questions",
            JSON_QUERY(@fees) AS "feeTypes",
            JSON_QUERY(@admissioncategories) AS "admissioncategories",
            JSON_QUERY(@campuses) AS "campuses",
            JSON_QUERY(@faculties) AS "faculties",
            JSON_QUERY(@programofferings) AS "programofferings",
            JSON_QUERY(@programs) AS "programs",
            JSON_QUERY(@terms) AS "terms"
        FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)
    END
    ELSE IF @UsageType='question'
    BEGIN
        SET @json = (SELECT JSON_QUERY(@ruleInfo) AS "ruleInfo", JSON_QUERY(@questions) AS "questions"
        FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)
    END
    ELSE IF @UsageType='fee'
    BEGIN
        SET @json = (SELECT JSON_QUERY(@ruleInfo) AS "ruleInfo", JSON_QUERY(@fees) AS "feeTypes"
        FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)
    END
    ELSE IF @UsageType='admissioncategory'
    BEGIN
        SET @json = (SELECT JSON_QUERY(@ruleInfo) AS "ruleInfo", JSON_QUERY(@admissioncategories) AS "admissioncategories"
        FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)
    END
    ELSE IF @UsageType='campus'
    BEGIN
        SET @json = (SELECT JSON_QUERY(@ruleInfo) AS "ruleInfo", JSON_QUERY(@campuses) AS "campuses"
        FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)
    END
    ELSE IF @UsageType='faculty'
     BEGIN
        SET @json = (SELECT JSON_QUERY(@ruleInfo) AS "ruleInfo", JSON_QUERY(@faculties) AS "faculties"
        FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)
    END
    ELSE IF @UsageType='programoffering'
    BEGIN
        SET @json = (SELECT JSON_QUERY(@ruleInfo) AS "ruleInfo", JSON_QUERY(@programofferings) AS "programofferings"
        FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)
    END
    ELSE IF @UsageType='program'
    BEGIN
        SET @json = (SELECT JSON_QUERY(@ruleInfo) AS "ruleInfo", JSON_QUERY(@programs) AS "programs"
        FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)
    END
    ELSE -- IF @UsageType='term'
        BEGIN
        SET @json = (SELECT JSON_QUERY(@ruleInfo) AS "ruleInfo", JSON_QUERY(@terms) AS "terms"
        FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)
    END

    SELECT @json AS "rule usage"
        
    END TRY
    BEGIN CATCH
    IF ERROR_NUMBER() > 50000
      THROW;
    
    DECLARE @ErrorMessage VARCHAR(400)
    SELECT @ErrorMessage = 'rule_usage failed Rule GUID ' + @RuleGUID + ': '  + ERROR_MESSAGE();
    THROW 50000, @ErrorMessage, 1;
    END CATCH
END;

-- +goose StatementEnd

-- +goose Down

DROP PROCEDURE [test].[GetRuleUsage];

