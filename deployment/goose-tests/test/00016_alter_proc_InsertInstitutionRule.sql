-- +goose Up

-- +goose StatementBegin

CREATE PROCEDURE [test].[InsertInstitutionRule]
  @SessionGUID VARCHAR(50),
  @EPBCID VARCHAR(50),
  @Name VARCHAR(50),
  @Description VARCHAR(256),
  @AST VARCHAR(8000),
  @Expr VARCHAR(8000),
  @EvalType VARCHAR(8)
WITH
  EXEC AS CALLER
AS
BEGIN
  DECLARE @BaseCode INT = 50000;
  DECLARE @ReturnCode INT;
  DECLARE @Message VARCHAR(200);

  BEGIN TRY

  IF NOT EXISTS (SELECT *
  FROM auth.USERS
  WHERE [GUID] = @SessionGUID)
  BEGIN
    SET @ReturnCode = @BaseCode + 404;
    SET @Message = 'session user is not found with GUID ' + @SessionGUID;
    THROW @ReturnCode, @Message, 1;
  END

  DECLARE @InstitutionID INT;
  SELECT @InstitutionID = ID
  FROM [inst].[INSTITUTIONS]
  WHERE EPBCID = @EPBCID;
  
  IF @InstitutionID IS NULL
  BEGIN
    SET @ReturnCode = @BaseCode + 404;
    SET @Message = 'institution not found with EPBCID ' + @EPBCID;
    THROW @ReturnCode, @Message, 1;
  END

  IF @EvalType = 'AST'
  BEGIN
    INSERT INTO [test].[RULES]
      (Description, AST, EvalType, Name, InstitutionID, CreateUser, Status)
    VALUES(@Description, @AST, @EvalType, @Name, @InstitutionID, @SessionGUID, 'DRAFT');
  END
  ELSE -- 'EXPR'
  BEGIN
    INSERT INTO [test].[RULES]
      (Description, Expr, EvalType, Name, InstitutionID, CreateUser, Status)
    VALUES(@Description, @Expr, @EvalType, @Name, @InstitutionID, @SessionGUID, 'DRAFT');
  END
  
  DECLARE @RuleID INT;
  SELECT @RuleID = SCOPE_IDENTITY();
    
  SELECT [r].GUID AS "guid",
    [r].Name AS "name",
    [r].Description AS "description",
    [r].AST AS "ast",
    [r].Expr AS "expr",
    [r].EvalType AS "evalType",
    [r].Status AS "status",
    FORMAT([r].Created, N'yyyy-MM-ddTHH:mm:ssZ') AS "updateDetails.created",
    [r].CreateUser AS "updateDetails.createUser",
    FORMAT([r].Updated, N'yyyy-MM-ddTHH:mm:ssZ') AS "updateDetails.updated",
    [r].UpdateUser AS "updateDetails.updateUser"
  FROM [test].[RULES] [r]
  WHERE [r].ID = @RuleID
  FOR JSON PATH, WITHOUT_ARRAY_WRAPPER;

  END TRY
  
  BEGIN CATCH
  IF ERROR_NUMBER() > 50000
  BEGIN
      ;
      THROW;
    END
  
  DECLARE @ErrorMessage VARCHAR(400)
  SELECT @ErrorMessage = 'insert rule failed for EPBCID '  + @EPBCID + ': '  + ERROR_MESSAGE();
  THROW 50000, @ErrorMessage, 1;
  END CATCH

END;


-- +goose StatementEnd

-- +goose Down

  DROP PROCEDURE [test].[InsertInstitutionRule];