-- +goose Up

-- +goose StatementBegin

CREATE PROCEDURE [test].[UpdateInstitutionRule]
    @EPBCID VARCHAR(50),
    @RuleGUID VARCHAR(50),
    @Description VARCHAR(256),
    @AST VARCHAR(8000),
    @Expr VARCHAR(8000),
    @EvalType VARCHAR(8),
    @Name VARCHAR(50),
    @Status VARCHAR(10),
    @SessionGUID VARCHAR(50)
WITH
    EXEC AS CALLER
AS
BEGIN
    DECLARE @BaseCode INT = 50000
    DECLARE @ReturnCode INT
    DECLARE @Message VARCHAR(200)

    BEGIN TRY
    IF NOT EXISTS
        (SELECT *
    FROM auth.USERS
    WHERE [GUID] = @SessionGUID)
    BEGIN
        SET @ReturnCode = @BaseCode + 404;
        SET @Message = 'session user is not found with GUID ' + @SessionGUID;
        THROW @ReturnCode, @Message, 1;
    END
        
    DECLARE @InstitutionID INT
    SELECT @InstitutionID = ID
    FROM [inst].[INSTITUTIONS]
    WHERE EPBCID = @EPBCID
        
    IF @InstitutionID IS NULL
    BEGIN
        SET @ReturnCode = @BaseCode + 404;
        SET @Message = 'institution not found with EPBCID ' + @EPBCID;
        THROW @ReturnCode, @Message, 1;
    END
            
    DECLARE @RuleID int
    SELECT @RuleID = ID
    FROM [test].[RULES]
    WHERE InstitutionID = @InstitutionID AND GUID = @RuleGUID
        
    IF @RuleID IS NULL
    BEGIN
        SET @ReturnCode = @BaseCode + 404;
        SET @Message = 'rule not found for institution ' + @EPBCID + ' with rule guid ' + @RuleGUID;
        THROW @ReturnCode, @Message, 1;
    END

    DECLARE @CURR_STATUS VARCHAR(10);
    SELECT @CURR_STATUS = Status
    FROM [test].[RULES]
    WHERE ID=@RuleID

    IF @CURR_STATUS='ARCHIVED'
    BEGIN
        SET @ReturnCode = @BaseCode + 400;
        SET @Message = 'cannot modify ARCHIVED rule for institution ' + @EPBCID + ' with rule guid ' + @RuleGUID;
        THROW @ReturnCode, @Message, 1;
    END

    IF @CURR_STATUS='FINAL'
    BEGIN
        IF @Status != 'ARCHIVED'
        BEGIN
            SET @ReturnCode = @BaseCode + 400;
            SET @Message = 'cannot modify rule in FINAL status for institution ' + @EPBCID + ' with rule guid ' + @RuleGUID;
            THROW @ReturnCode, @Message, 1;
        END
        ELSE
        BEGIN
            -- changing a FINAL status RULE to ARCHIVED forces all objects referencing it to also be set to ARCHIVED
            -- an ARCHIVED status RULE may only be referenced by objects also in ARCHIVED state
            -- for V1 disallow setting a RULE to ARCHIVED unless all objects referring to it are already ARCHIVED
            IF ( 
            (SELECT ID
                FROM fees.[FEE_TYPES]
                WHERE RuleID=@RuleID AND Status!='ARCHIVED'
                ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
                OR
                (SELECT QuestionID
                FROM bbq.[QUESTIONS]
                WHERE RuleID=@RuleID AND Status!='ARCHIVED'
                ORDER BY QuestionID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
                OR
                (SELECT ID
                FROM progsel.[ADMISSION_CATEGORIES]
                WHERE RuleID=@RuleID AND Status!='ARCHIVED'
                ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
                OR
                (SELECT ID
                FROM progsel.[CAMPUSES]
                WHERE RuleID=@RuleID AND Status!='ARCHIVED'
                ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
                OR
                (SELECT ID
                FROM progsel.[FACULTIES]
                WHERE RuleID=@RuleID AND Status!='ARCHIVED'
                ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
                OR
                (SELECT ID
                FROM progsel.[PROGRAM_OFFERINGS]
                WHERE RuleID=@RuleID AND Status!='ARCHIVED'
                ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
                OR
                (SELECT ID
                FROM progsel.[PROGRAMS]
                WHERE RuleID=@RuleID AND Status!='ARCHIVED'
                ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
                OR
                (SELECT ID
                FROM progsel.[TERMS]
                WHERE RuleID=@RuleID AND Status!='ARCHIVED'
                ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            )
            BEGIN
                SET @ReturnCode = @BaseCode + 400;
                SET @Message = 'cannot ARCHIVE rule until all database objects referencing it are archived for institution ' + @EPBCID + ' with rule guid ' + @RuleGUID;
                THROW @ReturnCode, @Message, 1;
            END
        END

        UPDATE [test].[RULES]
        SET
            [Status]='ARCHIVED',
            [UpdateUser] = @SessionGUID,
            [Updated] = getdate(),
            [ArchiveDate] = getdate()
        WHERE ID=@RuleID;
    END

    IF @CURR_STATUS='DRAFT'
    BEGIN
        -- a DRAFT rule may only be referenced by child objects that are also in DRAFT status
        IF ( 
            (SELECT ID
            FROM fees.[FEE_TYPES]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT QuestionID
            FROM bbq.[QUESTIONS]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY QuestionID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[ADMISSION_CATEGORIES]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[CAMPUSES]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[FACULTIES]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[PROGRAM_OFFERINGS]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[PROGRAMS]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            OR
            (SELECT ID
            FROM progsel.[TERMS]
            WHERE RuleID=@RuleID AND Status!='DRAFT'
            ORDER BY ID DESC OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY) IS NOT NULL
            )
        BEGIN
            SET @ReturnCode = @BaseCode + 500;
            SET @Message = 'the DRAFT rule for institution ' + @EPBCID + ' with rule guid ' + @RuleGUID + ' is referenced by database objects that are not in DRAFT which is forbidden';
            THROW @ReturnCode, @Message, 1;
        END

        UPDATE [test].[RULES]
        SET
            [Description] = @Description,
            [AST] = @AST,
            [Expr] = @Expr,
            [EvalType] = @EvalType,
            [Name] = @Name,
            [InstitutionID] = @InstitutionID,
            [Status] = @Status,
            [UpdateUser] = @SessionGUID,
            [Updated] = getdate()
        WHERE ID = @RuleID;
    END
                
    -- return rule JSON
    SELECT [r].GUID AS "guid",
        [r].Name AS "name",
        [r].Description AS "description",
        [r].AST AS "ast",
        [r].Expr AS "expr",
        [r].EvalType AS "evalType",
        [r].Status AS "status",
        FORMAT([r].Created, N'yyyy-MM-ddTHH:mm:ssZ') AS "updateDetails.created", [r].CreateUser AS "updateDetails.createUser",
        FORMAT([r].Updated, N'yyyy-MM-ddTHH:mm:ssZ') AS "updateDetails.updated", [r].UpdateUser AS "updateDetails.updateUser"
    FROM [test].[RULES] [r]
    WHERE [r].ID = @RuleID
    FOR JSON PATH, WITHOUT_ARRAY_WRAPPER

    END TRY
    
    BEGIN CATCH
    IF ERROR_NUMBER() > 50000
        THROW;
    DECLARE @ErrorMessage VARCHAR(400)
    SELECT @ErrorMessage = 'update institution rule failed for EPBCID '  + @EPBCID + ' and RuleGUID ' + @RuleGUID + ': '  + ERROR_MESSAGE();
    THROW 50000, @ErrorMessage, 1;
    END CATCH

END;

-- +goose StatementEnd

-- +goose Down

DROP PROCEDURE [test].UpdateInstitutionRule;

