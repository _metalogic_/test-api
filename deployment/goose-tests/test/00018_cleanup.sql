-- +goose Up

DROP PROCEDURE IF EXISTS [test].[DeleteRule];
DROP PROCEDURE IF EXISTS [test].[GetAllRules];
DROP PROCEDURE IF EXISTS [test].[GetOneRule];
DROP PROCEDURE IF EXISTS [test].[UpdateRule];

-- +goose Down

