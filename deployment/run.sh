#!/bin/bash

usage () {
  echo usage: run.sh env token
  if [ -z "$1" ]; then
    exit 1
  fi
  echo '---'
  echo $1
  exit 1
}

if [ $# != 2 ]; then
   usage
fi

env=$1

if [ $env == "prd" ]; then
   pre=""
elif [ $env == "dev" -o $env == "pvw" -o $env == "stg" -o $env == "tst" ]; then
   pre="${env}-"
else
   usage "env must be one of dev, prd, pvw, stg or tst"
fi

token=$2

set -ex

zip -j test_v1.0.zip v1.0/sql/${GOOSE_DRIVER}/test/*.sql 

curl --fail-with-body --location --request POST "https://${pre}apis.educationplannerbc.ca/deployment-api/v1/migrate/test_v1.0/up" \
--header "Authorization: Bearer $token" \
--header "Content-Type: application/zip" \
--data-binary "@test_v1.0.zip"

