-- +goose Up

/* a simple users table */

CREATE TABLE [test].[USERS]
(
    [ID] [int] IDENTITY(1,1) NOT NULL,
    username VARCHAR(200),
    name NVARCHAR(max),
    surname NVARCHAR(max)
);

-- with some sample users
INSERT INTO [test].[USERS]
VALUES
    ('root', '', ''),
    ('vojtechvitek', 'Vojtech', 'Vitek');

-- +goose Down
DROP TABLE [test].[USERS];

