-- +goose Up

-- +goose StatementBegin
CREATE OR ALTER PROCEDURE [test].[Delay]
@Duration CHAR(8)
AS
/* [test].[Delay] does a WAITFOR to simulate a long-running database operation */
BEGIN
  DECLARE @BaseCode INT = 50000
  DECLARE @ReturnCode INT
  DECLARE @Message VARCHAR(200)
  BEGIN TRY

    WAITFOR DELAY @Duration;  

    DECLARE @json NVARCHAR(max);
    SET @json = (SELECT 'waited ' + @Duration AS "message"
    FOR JSON PATH, WITHOUT_ARRAY_WRAPPER, INCLUDE_NULL_VALUES);

    SELECT @json;

  END TRY
  
  BEGIN CATCH
    IF ERROR_NUMBER() > 50000
      THROW;

    DECLARE @ErrorMessage VARCHAR(400);
    SELECT @ErrorMessage = 'Delay + ' + @Duration + ' failed: ' + ERROR_MESSAGE();
    THROW 50000, @ErrorMessage, 1; 
  END CATCH
END;
-- +goose StatementEnd

-- +goose Down

DROP PROCEDURE [test].[Delay];

