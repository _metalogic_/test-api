-- +goose Up

/* 
   Block comments may contain text that would be interpreted
   as a line comment:
   -- point 1
   -- point 2
*/

-- +goose StatementBegin
CREATE PROCEDURE [test].[Hello]
@Name VARCHAR(100)
AS
-- References: none
-- Referenced by: none
BEGIN
  DECLARE @BaseCode INT = 50000
  DECLARE @ReturnCode INT
  DECLARE @Message VARCHAR(200)
  BEGIN TRY
    /* 
    return Hello @Name JSON message
     */
    DECLARE @json NVARCHAR(max);
    SET @json = (SELECT 'Hello ' + @Name AS "message"
    FOR JSON PATH, WITHOUT_ARRAY_WRAPPER, INCLUDE_NULL_VALUES);

    SELECT @json;

  END TRY
  
  BEGIN CATCH
    IF ERROR_NUMBER() > 50000
      THROW;

    DECLARE @ErrorMessage VARCHAR(400);
    SELECT @ErrorMessage = 'Hello + ' + @Name + ' failed: ' + ERROR_MESSAGE();
    THROW 50000, @ErrorMessage, 1; 
  END CATCH
END;
-- +goose StatementEnd

-- +goose Down

/* 
   Block comments may contain text that would be interpreted
   as a line comment:
   -- point 1
   -- point 2
*/
DROP PROCEDURE [test].[Hello];

