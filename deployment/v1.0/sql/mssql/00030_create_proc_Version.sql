-- +goose Up

-- +goose StatementBegin

CREATE PROCEDURE [test].[Version] 
@Version varchar(400) OUTPUT
WITH EXEC AS CALLER
AS
BEGIN
  SELECT @Version = @@VERSION; -- version is a database builtin
END;

-- +goose StatementEnd

-- +goose Down

DROP PROCEDURE  [test].[Version];

