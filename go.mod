module bitbucket.org/_metalogic_/test-api

go 1.18

require (
	bitbucket.org/_metalogic_/config v1.4.0
	bitbucket.org/_metalogic_/glib v0.9.11
	bitbucket.org/_metalogic_/log v1.4.1
	github.com/denisenkom/go-mssqldb v0.12.2
	github.com/dimfeld/httptreemux/v5 v5.2.2
)

require (
	bitbucket.org/_metalogic_/color v1.0.4 // indirect
	bitbucket.org/_metalogic_/colorable v1.0.3 // indirect
	bitbucket.org/_metalogic_/isatty v1.0.4 // indirect
	github.com/golang-sql/civil v0.0.0-20190719163853-cb61b32ac6fe // indirect
	github.com/golang-sql/sqlexp v0.1.0 // indirect
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897 // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
)
