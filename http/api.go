package http

import (
	"context"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	. "bitbucket.org/_metalogic_/glib/http" // dot import fo avoid package prefix in reference (shutup lint)
	"bitbucket.org/_metalogic_/log"
	"bitbucket.org/_metalogic_/test-api"
)

func Test(method string) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {

		message := fmt.Sprintf("%s %s", method, r.URL)

		switch method {
		case "GET":
			MsgJSON(w, message)
		case "POST":
			digest := r.Header.Get("Digest")
			if digest == "" {
				ErrJSON(w, fmt.Errorf("digest header not found"))
				return
			}

			log.Debugf("got Digest %s", digest)

			sha, err := getDigest(r)
			if err != nil {
				ErrJSON(w, err)
				return
			}
			if digest != sha {
				ErrJSON(w, fmt.Errorf("digest %s != calculated %s", digest, sha))
				return
			}
			MsgJSON(w, message)
		case "PUT":
			MsgJSON(w, message)
		case "PATCH":
			MsgJSON(w, message)
		case "DELETE":
			MsgJSON(w, message)
		default:
			ErrJSON(w, fmt.Errorf("HTTP method %s not supported", method))
		}
	}
}

// @Summary executes a delay in the database
// @Description executes a delay in the database
// @Tags Test endpoints
// @Produce json
// @Param duration query string true "duration"
// @Success 200 {object} MessageResponse
// @Failure 400 {object} ErrorResponse
// @Failure 404 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /timeout [get]
func Delay(svc test.Service) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {

		duration, err := DurationParam(r, "duration", time.Second)
		if err != nil {
			ErrJSON(w, err)
			return
		}
		timeout, err := DurationHeader(r, "Timeout", time.Second)
		if err != nil {
			ErrJSON(w, err)
			return
		}

		ctx := context.Background()

		testJSON, err := svc.Timeout(ctx, timeout, duration)

		if err != nil {
			ErrJSON(w, err)
			return
		}

		log.Debug(testJSON)

		OkJSON(w, testJSON)
	}
}

func getDigest(r *http.Request) (digest string, err error) {
	var bodyBytes []byte

	if r.Body == nil {
		return digest, fmt.Errorf("empty request body")
	}
	bodyBytes, err = ioutil.ReadAll(r.Body)
	if err != nil {
		return digest, fmt.Errorf("error reading request body: %v", err)
	}

	d := sha256.Sum256(bodyBytes)
	return fmt.Sprintf("SHA-256=%s", base64.StdEncoding.EncodeToString(d[:])), nil
}
