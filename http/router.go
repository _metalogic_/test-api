package http

import (
	"net/http"

	"bitbucket.org/_metalogic_/test-api"
	"github.com/dimfeld/httptreemux/v5"
)

// Handler ...
type Handler struct {
	service test.Service
	handler *httptreemux.TreeMux
}

// ServeHTTP serves requests against the applications-api.
func (h *Handler) ServeHTTP(addr string) error {
	return http.ListenAndServe(addr, h.handler)
}

// NewHandler returns a new Handler for the servicej
func NewHandler(svc test.Service) *Handler {
	return &Handler{service: svc, handler: router(svc)}
}

// create the router for Service
func router(svc test.Service) *httptreemux.TreeMux {
	// initialize HTTP router
	treemux := httptreemux.New()
	api := treemux.NewGroup("/test-api/v1")
	api.GET("/tests", Test("GET"))
	api.POST("/tests", Test("POST"))
	api.GET("/tests/:id", Test("GET"))
	api.POST("/tests/:id", Test("POST"))
	api.PUT("/tests/:id", Test("PUT"))
	api.DELETE("/tests/:id", Test("DELETE"))

	api.GET("/timeout", Delay(svc))
	// common endpoints
	api.GET("/health", Health())

	return treemux
}
