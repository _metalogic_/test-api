package mssql

import (
	"context"
	"database/sql"
	"fmt"
	"net/url"
	"time"

	. "bitbucket.org/_metalogic_/glib/http" // dot import fo avoid package prefix in reference (shutup lint)
	"bitbucket.org/_metalogic_/log"
	mssql "github.com/denisenkom/go-mssqldb"
)

// Service implements the auth-api Service interface against Microsoft SQLServer
type Service struct {
	DB   *sql.DB
	info map[string]string
}

// New creates a new Service and sets the database
func New(server, port, database, user, password string) (svc *Service, err error) {
	svc = &Service{}
	values := url.Values{}
	values.Set("database", database)
	values.Set("app", "EPBC auth-api")
	u := &url.URL{
		Scheme: "sqlserver",
		User:   url.UserPassword(user, password),
		Host:   fmt.Sprintf("%s:%s", server, port),
		// Path:  instance, // if connecting to an instance instead of a port
		RawQuery: values.Encode(),
	}

	log.Debugf("sqlserver connection string: %s", u.Redacted())

	svc.DB, err = sql.Open("sqlserver", u.String())
	if err != nil {
		return svc, err
	}
	svc.info = make(map[string]string)
	svc.info["Type"] = "sqlserver"
	svc.info["Version"], err = svc.getVersion()
	if err != nil {
		log.Error(err.Error())
	}
	svc.info["Database"] = database
	svc.DB.SetConnMaxLifetime(300 * time.Second)
	svc.DB.SetMaxIdleConns(50)
	svc.DB.SetMaxOpenConns(50)

	return svc, err
}

// Close closes the DB connection
func (svc *Service) Close() {
	svc.DB.Close()
}

// Health checks to see if the DB is available.
func (svc *Service) Health() error {
	return svc.DB.Ping()
}

// Info returns information about the Service.
func (svc *Service) Info() (info map[string]string) {
	return svc.info
}

// Stats returns Service  statistics
func (svc *Service) Stats() string {
	dbstats := svc.DB.Stats()
	js := fmt.Sprintf("{\"MaxOpenConnections\": %d, \"OpenConnections\" : %d, \"InUse\": %d, \"Idle\": %d, \"WaitCount\": %d, \"WaitDuration\": %d, \"MaxIdleClosed\": %d, \"MaxLifetimeClosed\": %d}",
		dbstats.MaxOpenConnections,
		dbstats.OpenConnections,
		dbstats.InUse,
		dbstats.Idle,
		dbstats.WaitCount,
		dbstats.WaitDuration,
		dbstats.MaxIdleClosed,
		dbstats.MaxLifetimeClosed)
	return js
}

// Timeout returns a JSON info message if a database request with an execution time
// specified by [duration] completes before [timeout]
func (svc *Service) Timeout(ctx context.Context, timeout, duration time.Duration) (msgJSON string, err error) {
	var (
		rows *sql.Rows
	)

	dbDelay := durationString(duration)

	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	log.Debugf("waiting %s for database delay of %s", timeout, dbDelay)

	rows, err = svc.DB.QueryContext(ctx, "[test].[Delay]",
		sql.Named("Duration", dbDelay))

	if err != nil {
		return msgJSON, dbError(err)
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&msgJSON)
	}
	if err != nil {
		return msgJSON, NewDBError(err.Error())
	}
	return msgJSON, nil
}

func (svc *Service) getVersion() (version string, err error) {
	_, err = svc.DB.QueryContext(context.Background(), "[dbo].[Version]", sql.Named("Version", sql.Out{Dest: &(version)}))
	if err != nil {
		log.Errorf("%s", err)
		return version, err
	}
	return version, nil
}

func dbError(err error) error {
	if err == nil {
		return nil
	}
	if dberr, ok := err.(mssql.Error); ok {

		code := dberr.SQLErrorNumber()
		code = code - 50000

		switch code {
		case 400:
			return NewBadRequestError(dberr.SQLErrorMessage())
		case 404:
			return NewNotFoundError(dberr.SQLErrorMessage())
		case 500:
			return NewServerError(fmt.Sprintf("%s: %s", dberr.SQLErrorServerName(), dberr.SQLErrorMessage()))
		default:
			return NewBadRequestError(dberr.SQLErrorMessage())
		}
	}
	return NewServerError(err.Error())
}

func durationString(duration time.Duration) string {
	z := time.Unix(0, 0).UTC()
	return z.Add(duration).Format("15:04:05")
}
