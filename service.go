package test

import (
	"context"
	"time"
)

// Common defines the common service interface
type Common interface {
	Health() error
	Info() map[string]string
	Stats() string
}

// Service defines the Auth service interface
type Service interface {
	Common
	Timeout(ctx context.Context, timeout, duration time.Duration) (msgJSON string, err error)
}
